class Move {
    
    //constructeur de mouvements
    constructor(move, author, target) {
        this._move = parseInt(move, 10);
        this._author = author;
        this._target = target;
    }

    // ----- GETTERS -----

    get getMove(){
        return this._move;
    }

    get getAuthor(){
        return this._author;
    }

    get getTarget(){
        return this._target;
    }
}

module.exports = Move;