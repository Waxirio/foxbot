class Weapon {
    //constructeur d'armes
    constructor(nom, direct, blocage, estoc, parade, ecrasant, bouclier, taille, rempart) {
        //transformer les infos en entiers
        direct = parseInt(direct, 10);
        blocage = parseInt(blocage, 10);
        estoc = parseInt(estoc, 10);
        parade = parseInt(parade, 10);
        ecrasant = parseInt(ecrasant, 10);
        bouclier = parseInt(bouclier, 10);
        taille = parseInt(taille, 10);
        rempart = parseInt(rempart, 10);
        //nom de l'arme
        this._nom = nom;
        //valeurs de caractéristiques de l'arme
        this._direct = direct
        this._blocage = blocage
        this._estoc = estoc;
        this._parade = parade;
        this._ecrasant = ecrasant;
        this._bouclier = bouclier;
        this._taille = taille;
        this._rempart = rempart;
    }

    // ----- GETTERS -----

    get getNom(){
        return this._nom;
    }

    get getDirect(){
        return this._direct;
    }

    get getBlocage(){
        return this._blocage;
    }

    get getEstoc(){
        return this._estoc;
    }

    get getParade(){
        return this._parade;
    }

    get getEcrasant(){
        return this._ecrasant;
    }

    get getBouclier(){
        return this._bouclier;
    }

    get getTaille(){
        return this._taille;
    }

    get getRempart(){
        return this._rempart;
    }
}

module.exports = Weapon;