const Weapon = require('./weapon.js');

class Character {

    //constructeur de personnages
    constructor(nom, force, agilite, endurance, esprit, intuition, volonte) {
        //transformer les infos en entiers
        force = parseInt(force, 10);
        agilite = parseInt(agilite, 10);
        endurance = parseInt(endurance, 10);
        esprit = parseInt(esprit, 10);
        intuition = parseInt(intuition, 10);
        volonte = parseInt(volonte, 10);

        //caractéristiques primaires
        this._nom = nom;
        this._force = force;
        this._agilite = agilite
        this._endurance = endurance;
        this._esprit = esprit;
        this._intuition = intuition;
        this._volonte = volonte;

        //Caractéristiques secondaires
        this._sante = 20 + (volonte*3) + (force*2) + (endurance*5);
        this._santeMax = 20 + (volonte*3) + (force*2) + (endurance*5);
        this._mana = 20 + (volonte*5) + (esprit*4) + (intuition);
        this._manaMax = 20 + (volonte*5) + (esprit*4) + (intuition);
        this._esquive = agilite/2;
        this._resistanceSpi = volonte/2;
        this._resistanceEle = endurance/2;
        this._direct = force + intuition + endurance;
        this._blocage = force + intuition + endurance;
        this._estoc = force + esprit + agilite;
        this._parade = force + esprit + agilite
        this._ecrasant = (force*2) + endurance;
        this._bouclier = (force*2)+endurance; 
        this._taille = (force*2) + agilite;
        this._rempart = (force + (endurance*2) + intuition);
        this._derobade = (agilite*2) + intuition + esprit;
        this._feinte = (esprit*2) + intuition + esprit;
        this._deplacement = 5 + (((agilite*10) /3) / 10);

        //liste des armes du personnage
        this._weapon = null;

        //caractéristiquse autre
        this._armure = endurance;

    }

    // ----- GETTERS -----

    get getNom(){
        return this._nom;
    }

    get getForce(){
        return this._force;
    }

    get getAgilite(){
        return this._agilite;
    }

    get getEndurance(){
        return this._endurance;
    }

    get getEsprit(){
        return this._esprit;
    }

    get getIntuition(){
        return this._intuition;
    }

    get getVolonte(){
        return this._volonte;
    }

    get getSante(){
        return this._sante;
    }

    get getSanteMax(){
        return this._santeMax;
    }

    get getMana(){
        return this._mana;
    }

    get getManaMax(){
        return this._manaMax;
    }
    
    get getEsquive(){
        return this._esquive;
    }

    get getResSpi(){
        return this._resistanceSpi;
    }

    get getResEle(){
        return this._resistanceEle;
    }

    get getDirect(){
        return this._direct;
    }

    get getBlocage(){
        return this._blocage;
    }

    get getEstoc(){
        return this._estoc;
    }

    get getParade(){
        return this._parade;
    }

    get getEcrasant(){
        return this._ecrasant;
    }

    get getBouclier(){
        return this._bouclier;
    }

    get getTaille(){
        return this._taille;
    }

    get getRempart(){
        return this._rempart;
    }

    get getDerobade(){
        return this._derobade;
    }

    get getFeinte(){
        return this._feinte;
    }

    get getDeplacement(){
        return this._deplacement;
    }

    get getArmure(){
        return this._armure;
    }

    get getWeapon(){
        return this._weapon;
    }

    // ----- FUNCTIONS -----

    //se faire frapper d'un montant de dégats
    seFaireFrapper(amount){
        if(amount > (this._armure+this._esquive)){
            let degats = amount - this._armure - this._esquive;
            this._sante -= degats;
            if(this._sante < 0){
                this._sante = 0
            }
        }
    }

    //frapper d'un montant de dégats
    frapper(defense, amount, char){
        degats = amount - defense;
        char.seFaireFrapper(degats);
    }

    //check si le personnage est vivant ou pas
    estVivant(){
        return this._sante > 0;
    }

    //réinitialise les données du personnage après le combat
    finCombat(){
        this._sante = this._santeMax;
        this._mana = this._manaMax;
    }

    //set weapon
    setArme(w){
        this._weapon = w;
    }
}

module.exports = Character;