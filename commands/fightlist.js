/**********************************
 * DONNE LA LISTE DES COMBATTANTS * 
 **********************************/

const character = require('./../data/character.json');
const fighter = require('./../data/fighter.json');
const fight = require('./../data/fight.json');
const Character = require('./../class/character.js');
const Discord = require("discord.js");
const fs = require("fs");

exports.run = (client, message, args) => {
    //initialisation d'un string vide
    let fighterList = "";
    //boucler dans tous les combattants
    for (var key in fighter) {
        //récupérer la liste des combattants en leur attribuant un id
        fighterList += "- " + fighter[key] + "\n";
    }

    //si la liste est vide
    if(fighterList == ""){
        //on envoie un message pour dire que personne n'est la
        message.channel.send("aucun combattant n'est présent dans l'arène");
    }
    else{
        //sinon on crée un message riche avec la liste
        let embed = {
            "embed": {
                "title": `Les combattants présents sont :`,
                "description":  fighterList,
                "color": 16744271,
                "thumbnail": {
                    "url": "https://cdn.discordapp.com/avatars/629751665632739342/a3dbf2c89760d136d2725f43ce6da683.png"
                }
            }
        }
        //on envoie le message
        message.channel.send(embed);
    }
    
}