/*****************************
 *    ENLEVE UN COMBATTANT   * 
 *****************************/

const character = require('./../data/character.json');
const fighter = require('./../data/fighter.json');
const fight = require('./../data/fight.json');
const Character = require('./../class/character.js');
const Discord = require("discord.js");
const fs = require("fs");

exports.run = (client, message, args) => {
    //si le combat a pas encore commencé
    if(fight.begin == false){
        //recupérer l'id de celui qui envoie le message
        let id = message.author.id;
        //recupérer son personnage
        let p = Object.assign(new Character, character[id]);
        //enlever a la liste des combattants un personnage
        delete fighter[id];
        //sauvegarder la liste des combattants
        fs.writeFileSync('./data/fighter.json', JSON.stringify(fighter));
        //avertir le gars qu'il est dans l'arène
        message.channel.send(`${p.getNom} sort de l'arène de Molag Baal!`);
    }
    else{
        //le combat a déja commencé, on averti l'user
        message.channel.send(`L'arène est fermée, les combatants donnent leur maximum à l'intérieur...`);
    }
}