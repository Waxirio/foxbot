/*******************************
 *   RECHARGE UNE COMMANDE     * 
 *******************************/
exports.run = (client, message, args) => {
    //check des arguments donnés
    if(!args || args.length < 1) return message.reply("Donnez un nom de commande de recharger.");
    //get argument name
    const commandName = args[0];
    // Check if the command exists and is valid
    if(!client.commands.has(commandName)) {
      return message.reply("Cette commande n'existe pas");
    }
    // the path is relative to the *current folder*, so just ./filename.js
    delete require.cache[require.resolve(`./${commandName}.js`)];
    // We also need to delete and reload the command from the client.commands Enmap
    client.commands.delete(commandName);
    //reinclude the file
    const props = require(`./${commandName}.js`);
    //reset command
    client.commands.set(commandName, props);
    //avert that it worked
    message.reply(`La commande ${commandName} a été rechargée`);
  };