/*******************************
 *       LANCE UN COMBAT       * 
 *******************************/
const character = require('./../data/character.json');
const fighter = require('./../data/fighter.json');
const fight = require('./../data/fight.json');
const fMove = require('./../data/fightMove.json');
const Move = require('./../class/move.js');
const Character = require('./../class/character.js');
const Discord = require("discord.js");
const fs = require("fs");

exports.run = (client, message, args) => {
    //if there is at least 2 fighters
    if(Object.keys(fighter).length > 1){
        //if fight has not begun
        if(fight.begin == false){
            //set fight 
            fight.begin = true;
            message.channel.send("Les tribunes tremblent, les gens exités déchirent leur voisin et voisines à la vue des braves entrant sur le sable chaud sous un soleil crépusculaire");
            let personnages = "";
            let i = 1;
            for (var key in fighter) {
                fMove[key] = new Move('0','0','0');
                personnages += `**${i} : ** ${fighter[key]}\n`
                i ++;
            }
            fs.writeFileSync('./data/fightMove.json', JSON.stringify(fMove));
            fs.writeFileSync('./data/fight.json', JSON.stringify(fight));
    
            let embed = {
                "embed": {
                    "title": `A vous de jouer`,
                    "description":  `**Faites !move [nombre] [personnage] avec:**\n` +
                                    `**Pour [nombre]**\n` +
                                    `**1 :** direct\n` +
                                    `**2 :** blocage\n` +
                                    `**3 :** estoc\n` +
                                    `**4 :** parade\n` +
                                    `**5 :** ecrasant\n` +
                                    `**6 :** bouclier\n` +
                                    `**7 :** taille\n` +
                                    `**8 :** rempart\n` +
                                    `**9 :** derobade\n` +
                                    `**10 :** feinte\n`+
                                    `\n` +
                                    `**Pour [personnage]**\n` +
                                    personnages,
                    "color": 16744271,
                    "thumbnail": {
                    "url": "https://cdn.discordapp.com/avatars/629751665632739342/a3dbf2c89760d136d2725f43ce6da683.png"
                    },
                    "author": {
                    "name": `${message.author.username}`,
                    "icon_url": `${message.author.avatarURL}`
                    },
                }
            }
    
            for (var key in fighter) {
                let player = client.users.get(key);
                player.send(embed);
            }
        }
        else{
            message.channel.send("Le combat à déjà commencé !");
        }
    }
    else{
        message.channel.send("Pas assez de combattants");
    }
}