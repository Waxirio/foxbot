/*******************************
 * CREE UN NOUVEAU PERSONNAGE  * 
 *******************************/

const character = require('./../data/character.json');
const Character = require('./../class/character.js');
const Discord = require("discord.js");
const fs = require("fs");

exports.run = (client, message, args) => {
    //check les arguments en entrée
    if(!args || args.length < 7) return message.reply("Donnez 7 arguments");
    if(!isString(args[0]) || //nom
            isNaN(args[1]) || //force
            isNaN(args[2]) || //agilite
            isNaN(args[3]) || //endurance
            isNaN(args[4]) || //esprit
            isNaN(args[5]) || //intuition
            isNaN(args[6])){ //volonte
        message.channel.send(`Les données ne sont pas optimales`);
    }
    else{
        //si ils correspondent au premier test, passons au second
        if(isString(args[0]) &&
                Number.isInteger(parseInt(args[1], 10)) &&
                Number.isInteger(parseInt(args[2], 10)) && 
                Number.isInteger(parseInt(args[3], 10)) &&
                Number.isInteger(parseInt(args[4], 10)) && 
                Number.isInteger(parseInt(args[5], 10)) && 
                Number.isInteger(parseInt(args[6], 10))){
            //si ok, on crée le nouveau perso
            let p = new Character(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
            //on l'enregistre
            character[message.author.id] = p;
            
            //save character
            fs.writeFileSync('./data/character.json', JSON.stringify(character));
            
            //avert the user
            message.channel.send(`Personnage crée avec succès`);
        }
    }

    //check if a variable is a string
    function isString (value) {
        return typeof value === 'string' || value instanceof String;
    }

}