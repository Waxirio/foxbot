/************************************
 *    DONNE UNE ARME A UN PERSO     * 
 ************************************/

const weapon = require('./../data/weapon.json');
const Weapon = require('./../class/weapon.js');
const character = require('./../data/character.json');
const Character = require('./../class/character.js');
const Discord = require("discord.js");
const fs = require("fs");

exports.run = (client, message, args) => {
    //check des arguments donnés
    if(!args || args.length < 1) return message.reply("Donnez un id d'arme à donner");

    if(!isNaN(args[0]) && Number.isInteger(parseInt(args[0], 10))){
        //récupérons l'arme
        const nbWeapon = parseInt(args[0], 10) - 1;
        if(nbWeapon < Object.keys(weapon).length && nbWeapon >= 0){
            //transformer l'id en objet
            let p = Object.assign(new Character, character[message.author.id]);
            //set weapon
            p.setArme(nbWeapon);
            //reassign weapon
            character[message.author.id] = p;
            //save file
            fs.writeFileSync('./data/character.json', JSON.stringify(character));
            //on envoie le message
            message.channel.send("L'arme à été donnée");
        }
        else{
            message.channel.send("Impossible, l'id n'est pas valide");
        }
    }
    else{
        message.channel.send("Donnez moi un id d'arme");
    }
}