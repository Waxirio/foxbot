/*****************************
 * CHARGE LA LISTE DES ARMES * 
 *****************************/

const weapon = require('./../data/weapon.json');
const Weapon = require('./../class/weapon.js');
const lineByLine = require('n-readlines');
const Discord = require("discord.js");
const fs = require("fs");

exports.run = (client, message, args) =>{
    var lineReader = require('readline').createInterface({
        input: require('fs').createReadStream('./data/armurerie.txt')
    });
    
    let i = 0;
    let w;
    let s;

    lineReader.on('line', function (line) {
        //split line with ;
        s = line.split(';');
        //create new weapon
        w = new Weapon(s[0],s[1],s[2],s[3],s[4],s[5],s[6],s[7],s[8]);
        //yay
        weapon[i] = w;
        //save weapons
        fs.writeFileSync('./data/weapon.json', JSON.stringify(weapon));
        i ++;
    });
    
    //avert the user
    message.channel.send(`Les armes ont été chargés`);
}