/*****************************
 *  DONNE LA LISTE DES ARMES * 
 *****************************/

const weapon = require('./../data/weapon.json');
const Weapon = require('./../class/weapon.js');
const Discord = require("discord.js");
const fs = require("fs");

exports.run = (client, message, args) => {
    //initialisation d'un string vide
    let weaponList = "";
    let w;
    //boucler dans toutes les armes
    let i = 1;
    for (var key in weapon) {
        w = Object.assign(new Weapon, weapon[key]);
        //récupérer la liste des combattants en leur attribuant un id
        weaponList += `- ${i} : ${w.getNom} \n`;
        i++;
    }

    //si la liste est vide
    if(weaponList == ""){
        //on envoie un message pour dire qu'il n'y a pas d'armes
        message.channel.send("Aucune arme est disponible");
    }
    else{
        //sinon on crée un message riche avec la liste
        let embed = {
            "embed": {
                "title": `Les Armes de l'armurerie :`,
                "description":  weaponList,
                "color": 16744271,
                "thumbnail": {
                    "url": "https://cdn.discordapp.com/avatars/629751665632739342/a3dbf2c89760d136d2725f43ce6da683.png"
                }
            }
        }
        //on envoie le message
        message.channel.send(embed);
    }
    
}