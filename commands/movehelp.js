/*****************************
 *       HELP INFO MOVE      * 
 *****************************/
const Discord = require("discord.js");

exports.run = (client, message, args) => {
    //renvoie un message contenant un cookie
    let embed = {
        "embed": {
            "title": `Force des coups: `,
            "description":  `**Direct >** Ecrasant,Derobade\n` +
                            `**Blocage >** Estoc, Taille\n` +
                            `**Estoc >** Ecrasant,Taille,Derobade\n` +
                            `**Parade >** Estoc,Ecrasant\n` +
                            `**Ecrasant >** Blocage,Rempart,Bouclier\n` +
                            `**Bouclier >** Direct,Estoc\n` +
                            `**Taille >** Direct, Parade\n` +
                            `**Rempart >** /\n` +
                            `**Derobade >** /\n` +
                            `**Feinte >** Rempart,Blocage,Parade\n`,
            "color": 16744271,
            "thumbnail": {
                "url": "https://cdn.discordapp.com/avatars/629751665632739342/a3dbf2c89760d136d2725f43ce6da683.png"
            }
        }
    }
    //on envoie le message
    message.channel.send(embed);
}