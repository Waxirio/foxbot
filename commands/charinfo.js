/*****************************
 * DONNE LES INFO D'UN PERSO * 
 *****************************/

const character = require('./../data/character.json');
const Character = require('./../class/character.js');
const weapon = require('./../data/weapon.json');
const Weapon = require('./../class/weapon.js');
const Discord = require("discord.js");
const fs = require("fs");

exports.run = (client, message, args) =>{
    //initialiser l'id du user a celui qui envoie le message
    let id = message.author.id;
    //si la commande est suivie d'un @mention
    if(args[0] != undefined && args[0].startsWith('<@')){
        //l'id du user est celui qui est mentionné
        id = message.mentions.users.first().id;
    }
    //load character
    let p = Object.assign(new Character, character[id]);
    
    //si le personnage existe
    if(p != null){
        let w = Object.assign(new Weapon, weapon[p.getWeapon]);
        let weaponInfo =`**-- ${w.getNom} --**\n` +
                        `> **Direct :** ${w.getDirect}\n` +
                        `> **Blocage :** ${w.getBlocage}\n` +
                        `> **Estoc :** ${w.getEstoc}\n` +
                        `> **Parade :** ${w.getParade}\n` +
                        `> **Ecrasant :** ${w.getEcrasant}\n` +
                        `> **Bouclier :** ${w.getBouclier}\n` +
                        `> **Taille :** ${w.getTaille}\n` +
                        `> **Rempart :** ${w.getRempart}\n`;
        //on construit un message riche avec les infos du personnage
        let embed = {
        "embed": {
            "title": `Personnage : ${p.getNom}`,
            "description":  `**Sante :** ${p.getSante}/${p.getSanteMax}\n` +
                            `**Mana :** ${p.getMana}/${p.getManaMax}\n` +
                            `**Armure :** ${p.getArmure}\n` +
                            `**Force :** ${p.getForce}\n` +
                            `**Agilite :** ${p.getAgilite}\n` + 
                            `**Endurance :** ${p.getEndurance}\n` +
                            `**Esprit :** ${p.getEsprit}\n` + 
                            `**Intuition :** ${p.getIntuition}\n` +
                            `**Volonte :** ${p.getVolonte}\n` + 
                            `\n` + 
                            `**Esquive :** ${p.getEsquive}\n` +
                            `**Resistance Spi. :** ${p.getResSpi}\n` +
                            `**Resistance Ele. :** ${p.getResEle}\n` +
                            `**Direct :** ${p.getDirect}\n` +
                            `**Blocage :** ${p.getBlocage}\n` +
                            `**Estoc :** ${p.getEstoc}\n` +
                            `**Parade :** ${p.getParade}\n` +
                            `**Ecrasant :** ${p.getEcrasant}\n` +
                            `**Bouclier :** ${p.getBouclier}\n` +
                            `**Taille :** ${p.getTaille}\n` +
                            `**Rempart :** ${p.getRempart}\n` +
                            `**Derobade :** ${p.getDerobade}\n` +
                            `**Feinte :** ${p.getFeinte}\n` +
                            `**Deplacement :** ${p.getDeplacement}\n` +
                            `\n` +
                            weaponInfo,
            "color": 16744271,
            "thumbnail": {
            "url": "https://cdn.discordapp.com/avatars/629751665632739342/a3dbf2c89760d136d2725f43ce6da683.png"
            },
            "author": {
            "name": `${message.author.username}`,
            "icon_url": `${message.author.avatarURL}`
            },
        }
        }
        // On envoie le message dans le channel de base
        message.channel.send(embed);
    }
    else{
        //dire que le personnage n'est pas encore crée
        message.channel.send(`Vous n'avez pas de personnage, créez en un !`);
    }

}