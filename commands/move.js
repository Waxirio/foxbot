/**********************
 * LANCE UNE ATTAQUE  * 
 **********************/

const character = require('./../data/character.json');
const weapon = require('./../data/weapon.json');
const fighter = require('./../data/fighter.json');
const fight = require('./../data/fight.json');
const fMove = require('./../data/fightMove.json');
const config = require("./../auth.json");

const Move = require('./../class/move.js');
const Character = require('./../class/character.js');
const Weapon = require('./../class/weapon.js');

const Discord = require("discord.js");
const fs = require("fs");

exports.run = (client, message, args) => {
    //set nbfighter variable
    let fNumber = 0
    //get number of fighter
    for (var key in fighter) {
        fNumber += 1;
    }
    if (!Object.assign(new Character, character[message.author.id]).estVivant()) message.author.send("Votre personnage est mort");
    //si le combat a commencé et que le personnage n'est pas mort
    else if(fight.begin){
        //if there is 2 number as arguments
        if(args.length == 2){
            if(Number.isInteger(parseInt(args[0], 10)) && Number.isInteger(parseInt(args[1], 10)) 
                && args[1] <= fNumber && args[1] > 0
                && args[0] > 0 && args[0] < 11){
                //args[0] > move
                //args[1] > target
                let i = 1
                //get targetted character
                for (var key in fighter) {
                    if(i == args[1]){
                        args[1] = key;
                    }
                    i += 1;
                }
                //create new move
                let m = new Move(args[0], message.author.id ,args[1]);
                //save the move
                fMove[message.author.id] = m;
                //write the move
                fs.writeFileSync('./data/fightMove.json', JSON.stringify(fMove));
                //avert the user that it's k for him
                message.channel.send("Votre coup à été prit en compte");
                //see if turn can be launched
                let run = true;
                //scan moves to see if there is empty moves
                for (var key in fMove) {
                    let m = Object.assign(new Move, fMove[key]);
                    //if there is an empty move
                    if(m.getMove == 0){
                        //don't face problem of war
                        run = false;
                    }
                }

                //if all moves are ok
                if(run){
                    //go face some problems
                    battleProcess();
                }
            }
        }
        else{
            message.channel.send("J'ai besoin de savoir quel coup vous allez jouer et sur qui");
        }
    }
    else{
        message.channel.send("Le combat n'a pas encore débuté");
    }

    function battleProcess(){
        //battle process
        //parse moves to deal damages
        recap = "";
        defense = 0;
        nom = "";

        for (var key in fMove) {
            //coup de l'attaquant
            let m1 = Object.assign(new Move, fMove[key]);
            //coup du joueur visé
            let m2 = Object.assign(new Move, fMove[m1.getTarget]);
            //premier joueur
            let p1 = Object.assign(new Character, character[m1.getAuthor]);
            //second joueur
            let p2 = Object.assign(new Character, character[m1.getTarget]);
            //arme de l'attaquant
            let w1 = Object.assign(new Weapon, weapon[p1.getWeapon]);
            //l'arme du defenseur
            let w2 = Object.assign(new Weapon, weapon[p2.getWeapon]);

            degats = 0;
            //set specific actions for each moves CALCULATE AMOUNT DEALED
            switch (m1.getMove){
                case 1: //direct
                    wDirect = w1.getDirect || 0;
                    degats = p1.getDirect + wDirect;
                    if([5,9].includes(m2.getMove)){degats = Math.floor(degats * 1.5);}//coup critique 
                    nom = "Direct";
                    break;
                case 2: //blocage
                    wBlocage = w1.getBlocage || 0;
                    if([3,7].includes(m2.getMove)){degats = Math.floor((p1.getBlocage + wBlocage)/2);}//coup critique 
                    nom = "Blocage";
                    break;
                case 3: //estoc
                    wEstoc = w1.getEstoc || 0;
                    degats = p1.getEstoc + wEstoc;
                    if([5,7,9].includes(m2.getMove)){degats = Math.floor(degats * 1.5);}//coup critique 
                    nom = "Estoc";
                    break;
                case 4: //parade
                    wParade = w1.getParade || 0;
                    if([3,5].includes(m2.getMove)){degats = Math.floor((p1.getParade + wParade)/2);}//coup critique 
                    nom = "Parade";
                    break;
                case 5: //ecrasant
                    wEcrasant = w1.getEcrasant || 0;
                    degats = p1.getEcrasant + wEcrasant;
                    if([2,6,8].includes(m2.getMove)){degats = Math.floor(degats * 1.5);}//coup critique 
                    nom = "Ecrasant";
                    break;
                case 6: //bouclier
                    wBouclier = w1.getBouclier || 0;
                    degats = p1.getBouclier + wBouclier;
                    nom = "Bouclier";
                    break;
                case 7: //taille
                    wTaille = w1.getTaille || 0;
                    degats = p1.getTaille + wTaille;
                    if([1,4].includes(m2.getMove)){degats = Math.floor(degats * 1.5);} //coup critique
                    nom = "Taille";
                    break;
                case 8: //rempart
                    nom = "Rempart";
                    break;
                case 9: //derobade
                    nom = "Derobade";
                    break;
                case 10: //feinte
                    degats = Math.floor(p1.getFeinte*1.5);
                    if(![2,4,8].includes(m2.getMove)){degats = 0;} //si pas de coup critique alors sans effet
                    nom = "Feinte";
                    break;
                }

            if(m1.getMove != 10){
                if(m2.getMove == 6 && m2.getTarget == m1.getAuthor && [1,3].includes(m1.getMove)){ //bouclier
                    wBouclier = w2.getBouclier || 0;
                    defense = Math.floor((p2.getBouclier + wBouclier) / 2);
                }
                else if(m2.getMove == 8){ //rempart
                    wRempart = w2.getRempart || 0;
                    defense = p2.getRempart + wRempart;
                }
                else if(m2.getMove == 9){ //derobade
                    defense = p2.getDerobade;
                }
            }
            
            //frappe
            p1.frapper(defense, degats, p2);
            //get characters of the move
            character[m1.getAuthor] = p1;
            character[m1.getTarget] = p2;
            //save the character after turn
            fs.writeFileSync('./data/character.json', JSON.stringify(character));

            recap += `${p1.getNom} (${p1.getSante}/${p1.getSanteMax}) attaque ${p2.getNom} (${p2.getSante}/${p2.getSanteMax}) avec ${nom} de ${degats} \n`;
    }

        let embed = {
            "embed": {
                "title": `Recapitulatif du tour`,
                "description": recap,
                "color": 16744271,
                "thumbnail": {
                "url": "https://cdn.discordapp.com/avatars/629751665632739342/a3dbf2c89760d136d2725f43ce6da683.png"
                }
            }
        }

        for (var key in fighter) {
            //get player
            let player = client.users.get(key);
            //send rich text
            player.send(embed);
        }
        client.channels.get(config.combat).send(embed);

        //set winner if there is one
        let winner;
        let alive = 0;
        for (var key in fighter) {
            //for each fighters
            let p = Object.assign(new Character, character[key]);
            //if the fighter is alive
            if(p.estVivant()){
                //count him
                alive ++;
                //set winner as a living one :)
                winner = key;
            }
        }

        //if there is only one fighter alive or none (row match)
        if(alive <= 1){
            //send message to each user in fight
            for (var key in fighter) {
                //get player
                let player = client.users.get(key);
                //send winner infos
                player.send(`FIN DU COMBAT, LE GAGNANT EST : ${client.users.get(winner)}`);
                //get user's character
                let p = Object.assign(new Character, character[key]);
                //reinit thir carac
                p.finCombat();
                //save it
                character[key] = p;
            }
            //send the result in the 'fighting' channel
            client.channels.get(config.combat).send(`FIN DU COMBAT, LE GAGNANT EST : ${client.users.get(winner)}`);
            //write character object
            fs.writeFileSync('./data/character.json', JSON.stringify(character));

            //save a blank move object for a new fight
            for (var key in fMove) {
                delete fMove[key];
            }
            fs.writeFileSync('./data/fightMove.json', JSON.stringify(fMove));

            //save a blank fighter object for new fight
            for (var key in fighter) {
                delete fighter[key];
            }
            fs.writeFileSync('./data/fighter.json', JSON.stringify(fighter));

            //stop the fight
            fight.begin = false;
            //save fight status
            fs.writeFileSync('./data/fight.json', JSON.stringify(fight));
        }
        else{
            newTurn();
        }
    }

    function newTurn(){
        //reinit moves
        let personnages = "";
        let i = 1;
        for (var key in fighter) {
            //set new empty moves
            if(fMove[key] != null){
                fMove[key] = new Move('0','0','0');
                personnages += `**${i} : ** ${fighter[key]}\n`
            }
            i ++;
        }

        //save new empry moves
        fs.writeFileSync('./data/fightMove.json', JSON.stringify(fMove));

        //resend message to fighterss
        let embed = {
            "embed": {
                "title": `A vous de jouer`,
                "description":  `**Faites !move [nombre] [personnage] avec:**\n` +
                                `**Pour [nombre]**\n` +
                                `**1 :** direct\n` +
                                `**2 :** blocage\n` +
                                `**3 :** estoc\n` +
                                `**4 :** parade\n` +
                                `**5 :** ecrasant\n` +
                                `**6 :** bouclier\n` +
                                `**7 :** taille\n` +
                                `**8 :** rempart\n` +
                                `**9 :** derobade\n` +
                                `**10 :** feinte\n`+
                                `\n` +
                                `**Pour [personnage]**\n` +
                                personnages,
                "color": 16744271,
                "thumbnail": {
                "url": "https://cdn.discordapp.com/avatars/629751665632739342/a3dbf2c89760d136d2725f43ce6da683.png"
                }
            }
        }

        //send new turn infos
        for (var key in fighter) {
            //get player
            let player = client.users.get(key);
            //end of the turn
            player.send("TOUR TERMINE");
            //send rich text
            player.send(embed);
        }
    }
    
}