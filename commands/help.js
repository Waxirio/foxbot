/********************************
 * DONNE LES INFO DES COMMANDES * 
 ********************************/

exports.run = (client, message, args) =>{
  //Crée un message riche avec les commandes
  let embed = {
      "embed": {
        "title": `Voici les commandes disponibles :`,
        "description": `**!cookie :** don de cookie\n` + 
                        `**!reload [cmd] :** recharge la commande [cmd]\n` +
                        `**!newchar [nom] [force] [agilite] [endurance] [esprit] [intuition] [volonte] :** Crée un nouveau perso (!! créer un nouveau supprimera l'ancien !!)\n` + 
                        `**!charinfo :** donne les infos de votre perso\n` +
                        `**!fight :** engage son personnage dans la prochaine bataille\n` +
                        `**!notfight :** enlève son personnage de la prochaine bataille\n` +
                        `**!fightlist :** donne la liste des combattants actuels\n` +
                        `**!startfight :** lance le combat\n` +
                        `**!move [idMove] [idPerso] :** lance une attaque\n` +
                        `**!wload :** charge la liste des armes\n` +
                        `**!wlist :** donne la liste des armes de l'armurerie\n` +
                        `**!wset [id] :** donne l'arme d'id [id] à votre personnage\n` +
                        `**!winfo [id] :** donne des infos sur une arme en particulier`,
        "color": 16744271,
        "thumbnail": {
          "url": "https://cdn.discordapp.com/avatars/629751665632739342/a3dbf2c89760d136d2725f43ce6da683.png"
        },
        "author": {
          "name": `${message.author.username}`,
          "icon_url": `${message.author.avatarURL}`
        },
      }
    }
    //envoie le message
    message.channel.send(embed);

}