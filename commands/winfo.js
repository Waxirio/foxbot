/************************************
 *   DONNE LES INFOS D'UNE ARME     * 
 ************************************/

const weapon = require('./../data/weapon.json');
const Weapon = require('./../class/weapon.js');
const Discord = require("discord.js");
const fs = require("fs");

exports.run = (client, message, args) => {
    //check des arguments donnés
    if(!args || args.length < 1) return message.reply("Donnez un id d'arme à inspecter");

    if(!isNaN(args[0]) && Number.isInteger(parseInt(args[0], 10))){
        //récupérons l'arme
        const nbWeapon = parseInt(args[0], 10) - 1;
        if(nbWeapon < Object.keys(weapon).length && nbWeapon >= 0){
            //transformer l'id en objet
            let w = Object.assign(new Weapon, weapon[nbWeapon]);
            //
            let embed = {
                "embed": {
                    "title": `Voici : ${w.getNom}`,
                    "description":  `**Direct :** ${w.getDirect}\n` +
                                    `**Blocage :** ${w.getBlocage}\n` +
                                    `**Estoc :** ${w.getEstoc}\n` +
                                    `**Parade :** ${w.getParade}\n` +
                                    `**Ecrasant :** ${w.getEcrasant}\n` +
                                    `**Bouclier :** ${w.getBouclier}\n` +
                                    `**Taille :** ${w.getTaille}\n` +
                                    `**Rempart :** ${w.getRempart}\n`,
                    "color": 16744271,
                    "thumbnail": {
                        "url": "https://cdn.discordapp.com/avatars/629751665632739342/a3dbf2c89760d136d2725f43ce6da683.png"
                    }
                }
            }
            //on envoie le message
            message.channel.send(embed);
        }
        else{
            message.channel.send("Impossible, l'id n'est pas valide");
        }
    }
    else{
        message.channel.send("Donnez moi un id d'arme");
    }
}